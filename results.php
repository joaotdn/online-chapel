<!doctype html>
<?php
	/**
	 * Require connect
	 */
	require_once 'connect.php';
?>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Capela Online</title>

	<link rel="stylesheet" href="stylesheets/bootstrap.min.css">
	<link rel="stylesheet" href="stylesheets/bootstrap-theme.min.css">
	<link rel="stylesheet" href="stylesheets/styles.css">
</head>
<body>
<?php if(!$db): ?>
	<!-- Connect error -->
	<div class="container">
		<h2 class="text-danger">Erro ao estabelecer conexão com o banco de dados</h2>
	</div>

<?php exit; else: ?>
	
	<?php
		mysql_select_db($db_nome);
		$query = "SELECT * FROM `velas_esperando` LIMIT 100";
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
	?>

	<div class="container">
		<div class="col-md-12">
			<h2><a href="index.php">Capela Online</a> <div class="pull-right"><button class="erase btn btn-primary btn-lg"  data-toggle="modal" data-target="#alert-del">Apagar velas com 7 dias</button></div></h2>
			<p class="text-primary upp"><em><?php echo $num_results; ?></em> resultados <div class="pull-right"><small><a href="#" class="text-danger waiting">Em espera</a></small><small><a href="#" class="lighting">Acesas</a></small></div></p>
		</div>

		<div class="col-md-12">
			<table id="table-msg" class="table table-hover">
				<thead>
					<tr class="warning">
						<td>Data</td>
						<td>Nome</td>
						<td>Email</td>
						<td colspan="2">Mensagem</td>
						<td>Ação</td>
					</tr>
				</thead>
				<tbody class="wait">
					<?php
						/**
						 * Init loop waiting messages
						 * @var integer
						 */
						for($i = 0; $i < $num_results; $i++) {
						$row = mysql_fetch_array($result);
					?>
					<tr>
						<td><?php print date('d\/m\/Y', strtotime($row['data'])); ?></td>
						<td><?php echo htmlspecialchars($row['nome']); ?></td>
						<td><?php echo htmlspecialchars($row['email']); ?></td>
						<td colspan="2"><?php echo htmlspecialchars($row['mensagem']); ?></td>
						<td>
							<button data-messageid="<?php echo htmlspecialchars($row['messageid']); ?>" class="delete-row btn btn-danger" data-toggle="modal" data-target="#alert-del">Excluir</button>
							<button data-date="<?php echo htmlspecialchars($row['data']); ?>" data-messageid="<?php echo htmlspecialchars($row['messageid']); ?>" class="add-row btn btn-success" data-toggle="modal" data-target="#alert-del">Aprovar</button>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
		
		<!-- aviso -->
		<div class="modal fade" id="alert-del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  			<div class="modal-dialog">
    			<div class="modal-content">
      				<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        				<h4 class="modal-title" id="myModalLabel">Ação</h4>
      				</div>
      				<div class="modal-body">
      				</div>
      				<div class="modal-footer">
        				<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      				</div>
    			</div><!-- /.modal-content -->
  			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

	</div>

<?php endif; ?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins.js"></script>

</body>
</html>