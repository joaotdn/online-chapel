	/**
	 * Esta função irá armazenar e tartar dos dados
	 * de cada mensagem por meio dos botões excluir e aprovar     
	 */
	
	function getAction() {
		var delData = $('.delete-row'),
			apData  = $('.add-row'),
			icon    = $('<div class="loading"><img src="img/ajax-loader.gif" alt=""></div>'),
			res     = $('em','.text-primary'),
			numRes  = parseInt(res.text());

		$.ajaxSetup({
			dataType: 'html',
			type: 'GET'
		});

		function delCandle() {
			var fId = $(this).data('messageid'),
				pa = $(this).parents('tr'),
				aId = $(this).data('apid');

			$.ajax({
				url: 'delete.php',
				data: {apId: aId, delId: fId},
				beforeSend: function() {
					pa.addClass('danger');
				},
				success: function(data,textStatus) {
					$('.modal-body','#alert-del').html('<p>Esta vela foi excluída</p>');
					if(numRes > 0) {
						res.html(numRes - 1);
					}
					pa.fadeOut('slow');
				},
				error: function(xhr,er) {
					if(er === 'timeout') {
						$('.modal-body','#alert-del').html('<p>Ocorreu um erro de Timeout. Tente novamente.</p>');
					} else {
						$('.modal-body','#alert-del').html('<p>Ocorreu um erro interno. Tente novamente.</p>');
					}
				}
			});
		};
		
		$(delData).bind('click',delCandle);

		function apCandle() {
			var fData = $(this).data('date'),
				fNome = $(this).parents('tr').find('td').eq(1).text(),
				fEmail = $(this).parents('tr').find('td').eq(2).text(),
				fMsg = $(this).parents('tr').find('td').eq(3).text(),
				fId = $(this).data('messageid'),
				pa = $(this).parents('tr');

			$.ajax({
				url: 'insert.php',
				data: {data:fData, nome:fNome, email: fEmail, msg: fMsg, apId: fId},
				beforeSend: function() {
					pa.addClass('success');
				},
				success: function(data,textStatus) {
					$('.modal-body','#alert-del').html('<p>Esta vela foi acesa</p>');
					res.html(numRes - 1);
					pa.fadeOut('slow');
				},
				error: function(xhr,er) {
					if(er === 'timeout') {
						$('.modal-body','#alert-del').html('<p>Ocorreu um erro de Timeout. Tente novamente.</p>');
					} else {
						$('.modal-body','#alert-del').html('<p>Ocorreu um erro interno. Tente novamente.</p>');
					}
				}
			});
		};
		
		$(apData).bind('click',apCandle);

		$('.lighting').bind('click',function(e) {
			e.preventDefault();
			$.ajax({
				url: 'light.php',
				data: {},
				beforeSend: function() {
					$('tbody','#table-msg').fadeOut('fast');
				},
				success: function(data,textStatus) {
					$('tbody','#table-msg').html(data).fadeIn('fast');
					var delData = $('.btn-danger'),
						res     = $('em','.text-primary'),
						numRes  = parseInt(res.text());

					delCandle();
					$(delData).bind('click',delCandle);

					var numRows = $('tr','#table-msg').length - 1;
					res.html(numRows);
				},
				error: function(xhr,er) {
					if(er === 'timeout') {
						alert('Ocorreu um erro de Timeout. Tente novamente.');
					} else {
						alert('Ocorreu um erro interno. Tente novamente.');
					}
				}
			});
		});

		$('.waiting').bind('click',function() {
			location.reload();
		});

		$('.erase').bind('click',function() {
			$(this).text('Velas de 7 dias apagadas').removeClass('btn-primary').addClass('btn-default');
			$.get('blackout.php',{},function(data) {
				$('.modal-body','#alert-del').html('<p>As velas com 7 dias foram apagadas!</p>');
			});
		})
	};
	getAction();

	/**
	 * Calcular e exibir o tempo da postagem
	 * @return {String} Os dias que as velas estão acesas
	 */
	function calcDia() {
		
		Date.prototype.dayofYear= function(){
    		var d= new Date(this.getFullYear(), 0, 0);
    		return Math.floor((this-d)/8.64e+7);
		}

		$.each($('li','.list-inline'), function(i,v) {
			var	now = new Date(),
				dt = $(this).data('day'),
				dy = now.dayofYear(),
				dd;

			if(dt > dy && dt - dy != 1) {
				dd = dt - dy + ' dias';
			} else if(dy > dt && dy - dt != 1) {
				dd = dy - dt + ' dias';
			} else if(dt - dy == 1) {
				dd = '1 dia';
			} else if(dt - dy == 0 || dy - dt == 0) {
				dd = 'hoje';
			}


			$('.text-muted',this).html('Esta vela foi acesa a <strong>' + dd + '</strong>');
		});
	};
	calcDia();