create table velas_esperando (
	messageid int unsigned not null auto_increment primary key,
	nome char(100) not null,
	email char(100) not null,
	data date not null auto_increment,
	mensagem char(250) not null
);

create table velas_acesas (
	messageid int unsigned not null auto_increment primary key,
	nome char(100) not null,
	email char(100) not null,
	data date not null auto_increment,
	mensagem char(250) not null,
	dia_ano char(3) not null
);

create table login_capela (
	nome char(25) NOT NULL,
	senha char(25) NOT NULL
);

INSERT INTO `login_capela` (`nome`,`senha`) VALUES ('admcapela', 'admin2072');