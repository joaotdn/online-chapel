<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Capela Online</title>

	<link rel="stylesheet" href="stylesheets/bootstrap.min.css">
	<link rel="stylesheet" href="stylesheets/bootstrap-theme.min.css">
	<link rel="stylesheet" href="stylesheets/styles.css">
</head>
<body>
	<div class="container">
		<div class="col-md-6 col-md-offset-3">
			<h2>Capela Online</h2>
			<p>Área administrativa</p>
			
			<form action="sigin.php" method="post" class="form-signin" role="form">
        		<input name="email" type="text" class="form-control" placeholder="Nome de usuário" required autofocus>
        		<input name="senha" type="password" class="form-control" placeholder="Senha" required>
        		<label class="checkbox">
          			<input type="checkbox" value="remember-me"> Lembrar
        		</label>
        		<button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
      		</form>
		</div>
	</div>
</body>
</html>