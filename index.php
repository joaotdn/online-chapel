<!doctype html>
<?php
	/**
	 * Require connect
	 */
	require_once 'connect.php';
?>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Capela Online</title>

	<link rel="stylesheet" href="stylesheets/bootstrap.min.css">
	<link rel="stylesheet" href="stylesheets/bootstrap-theme.min.css">
	<link rel="stylesheet" href="stylesheets/styles.css">
	<style>
		body {
			background: #FAE2F0 url(http://paroquia.webpb.com.br/wp-content/themes/paroquia/images/lines.png);
		}
		.btl {
			text-align: center;
		}
	</style>
</head>
<body>
<?php if(!$db): ?>
	<!-- Connect error -->
	<div class="container">
		<h2 class="text-danger">Erro ao estabelecer conexão com o banco de dados</h2>
	</div>

<?php exit; else: ?>
	
	<?php
		mysql_select_db($db_nome);
		$query = "SELECT * FROM `velas_acesas` LIMIT 150";
		$result = mysql_query($query);
		$num_results = mysql_num_rows($result);
	?>

	<div class="container">
		<header class="alert alert-danger col-md-12">
			<div class="col-md-6">
				<h1 class="navbar-text">Capela Online</h1>
			</div>
			
			<div class="col-md-12">
				<p class="lead text-center text-primary">Aqui você pode acender uma Vela Virtual e rezar pelas graças que deseja alcançar, ou pelas graças que já foram alcançadas</p>
			</div>

			<div class="col-md-12 btl">
				<button class="btn btn-success btn-lg"  data-toggle="modal" data-target="#form-light">Acenda uma vela</button>
			</div>
		</header>
		<ul class="list-inline">
			<?php
				/**
				 * Init loop agreed messages
				 * @var integer
				 */
				for($i = 0; $i < $num_results; $i++) {
				$row = mysql_fetch_array($result);
			?>
			<li data-day="<?php echo htmlspecialchars($row['dia_ano']); ?>" class="col-xs-6 col-sm-3 alert alert-warning">
				<figure><img src="img/vela.gif" alt=""></figure>
				<h3><?php echo htmlspecialchars($row['nome']); ?></h3>
				<p class="text-muted"></p>
			</li>
			<?php } ?>
		</ul>

		<div class="modal fade" id="form-light" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  			<div class="modal-dialog">
    			<div class="modal-content">
      				<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        				<h4 class="modal-title" id="myModalLabel">Acenda uma vela</h4>
      				</div>
      				<div class="modal-body">
      					<form id="form-candle" role="form" action="add_candle.php" method="post">
  							<div class="form-group">
    							<label for="exampleInputEmail1">Seu Nome</label>
    							<input type="text" name="nome" class="form-control" id="exampleInputEmail1" placeholder="Seu nome">
  							</div>
  							
  							<div class="form-group">
    							<label for="exampleInputPassword1">Seu e-mail</label>
    							<input type="email" name="email" class="form-control" id="exampleInputPassword1" placeholder="Digite seu e-mail">
  							</div>

  							<div class="form-group">
								<label for="exampleInputPassword1">Mensagem</label>
								<textarea name="mensagem" class="form-control" id="exampleInputPassword1" rows="3"></textarea>
  							</div>

  							<div class="modal-footer">
						 		<button type="submit" class="submit btn btn-default">Enviar</button>
							</div>
						</form>

						
      				</div>
    			</div><!-- /.modal-content -->
  			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div>

<?php endif; ?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script> 
<script src="js/plugins.js"></script>
</body>
</html>